class Node {
  //creates a new item that is a linked list object
  var data;
  var prev;
  var next;

  Node([this.data, this.prev, this.next]);
}

class LinkedList<T> {
  var head;
  var tail;
  var length;

  LinkedList([this.head, this.tail, this.length = 0]);

  @override
  String toString() {
    String nodesStr = "";
    var current = head;
    for (int i = 0; i < length; i++) {
      nodesStr += current.data.toString();
      if (i != length - 1) nodesStr += ", ";
      current = current.next;
    }
    String str = "[$nodesStr]";
    return str;
  }

  append(T data) {
    var current = Node(data);

    if (head == null) {
      head = current;
      tail = current;
    } else {
      current.prev = tail;
      tail.next = current;
      tail = current;
    }
    length++;
  }

  remove(T data) {
    var current = head;

    while (current != null) {
      if (current.data == data) {
        if (current.prev != null)
          current.prev.next = current.next;
        else
          head = current.next;

        if (current.next != null)
          current.next.prev = current.prev;
        else
          tail = current.prev;

        current.prev = null;
        current.next = null;
        length--;
        break;
      } else {
        current = current.next;
      }
    }
  }

  removeAt(int index) {
    var current = head;
    int counter = 0;

    while (counter <= length - 1) {
      if (counter == index) {
        if (current.prev != null)
          current.prev.next = current.next;
        else
          head = current.next;

        if (current.next != null)
          current.next.prev = current.prev;
        else
          tail = current.prev;

        current.prev = null;
        current.next = null;
        length--;
        break;
      } else {
        current = current.next;
        counter++;
      }
    }
  }

  setAt(int index, T data) {
    var current = Node(data);
    var neighbour;

    if (head == null) {
      head = current;
      tail = current;
    } else if (length == 1) {
      head = current;
      head.next = tail;
      tail.prev = head;
    } else if (index == 0) {
      current.next = head;
      head.prev = current;
      head = current;
    } else if (index == length - 1) {
      current.next = tail;
      current.prev = tail.prev;
      tail.prev.next = current;
      tail.prev = current;
    } else {
      neighbour = head;
      for (int i = 0; i < length; i++) {
        if (i == index) {
          current.next = neighbour;
          current.prev = neighbour.prev;
          neighbour.prev.next = current;
          neighbour.prev = current;
          break;
        }
        neighbour = neighbour.next;
      }
    }

    length++;
  }
}

main() {
  LinkedList<String> l = LinkedList<String>();
  l.append("0");
  l.setAt(0, "1");
  l.setAt(1, "5");
  l.setAt(1, "4");
  l.setAt(2, "11");
  l.setAt(0, "7");
  l.setAt(5, "4");
  l.setAt(6, "7");
  l.setAt(6, "7");
  l.append("55");
  l.removeAt(3);
  l.removeAt(8);
  l.removeAt(0);
  l.remove("0");
  l.remove("4");
  print(l.length);
  l.append("1");
  l.setAt(0, "0");
  l.setAt(1, "2");
  print(l);
  l.setAt(2, "3");
  print(l);
  l.setAt(1, "1.5");
  print(l);
}
