import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatelessWidget {
  final String helloText;

  MyHomePage({Key key, this.helloText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(helloText),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            child: Center(child: Text("Hello")),
            color: Colors.red,
            width: 200,
            height: 200,
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Container(
                    child: Center(child: Text("Hello")),
                    color: Colors.grey,
                    width: 100,
                    height: 100),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          try {
            Response<String> response = await Dio().post(
                "http://192.168.2.139:8081/table/Khomich/",
                queryParameters: {
                  "order": "Hello",
                });
            var json = jsonDecode(response.data);
            if('error' == json['status']){
              print(json['text']);
            }
            print(json['status']);
            print(json['text']);
            print(json['table']);
//            print(response);
          } catch (e) {
            print(e);
          }
        },
      ),
    );
  }
}
