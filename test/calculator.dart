import 'dart:io';
import 'dart:math';

main() {
  double a, b, c, d;

  a = double.parse(stdin.readLineSync());
  b = double.parse(stdin.readLineSync());
  c = double.parse(stdin.readLineSync());

  d = b * b - 4 * a * c;

  double x1 = (-b + sqrt(d)) / 2 * a;
  double x2 = (-b - sqrt(d)) / 2 * a;

  if (d >= 0) {
    print("x1: $x1");
    print("x2: $x2");
  } else
    print("Корни комплексные");
}
